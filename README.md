# tgbot-sismove #

Bot que publica en un grupo/canal/usuario cada vez que funvisis publica un sismo nuevo

[Canal de Telegram](https://t.me/joinchat/AAAAAEJXSsCWnafdokiAEA)

![Ejemplo](http://i.imgur.com/bj3SonP.png)

### Configurar ###

Hay que agregar los siguientes parametros

* $token: token del bot, el que se genera cuando creas el bot
* $channel: id del canal/grupo/usuario al que quieres enviar la informaci�n
* $here_id: id de la app que creas desde Here
* $here_code: c�digo de la app que creas desde here

El ID y C�digo lo puedes crear desde [Here](https://developer.here.com/)

Crear una tarea en el cron de la siguiente manera

    */1 * * * * cd /RUTA/DEL/SCRIPT/ && php sismo.php > /tmp/sismo.log 2>&1